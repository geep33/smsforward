# SMS Forward

SMS Forward is a simple app that allows you to redirect SMS you receive on your phone to another device.

It runs on Android 5.0 "Lollipop" (API 21) and later.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.pierreduchemin.smsforward/)

## Build instructions

Build with:
```bash
./gradlew assembleRelease
```

## Contributing

Any contributions welcome. Please fork this repository and create a pull request notifying your changes and why.

## Author

**Pierre Duchemin**

## License

SMS Forward's [LICENSE](LICENSE) is GNU GPL v3+.
